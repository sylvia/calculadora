
# ---- CALCULADORA.

def sumar(numero1, numero2):
    suma = numero1 + numero2
    print("La suma de los numeros", str(numero1), "y", str(numero2), "es:", suma)


def restar(numero1, numero2):
    if numero1 > numero2:
        resta = numero1 - numero2
    else:
        resta = numero2 - numero1

    print("La resta de los numeros", str(numero1), "y", str(numero2), "es:", resta)


if __name__ == "__main__":
    sumar(1, 2)
    sumar(3, 4)
    restar(5, 6)
    restar(7, 8)
